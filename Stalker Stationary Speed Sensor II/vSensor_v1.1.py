"""
CONFIGURACION LECTURA DATOS STALKER SENSOR II

@Author Aplik S.A by Yehad Rabba

-----------------HARDWARE------------------------

Conexion RS-232 [COM B] --> Adaptador USB


-----------------SOFTWARE------------------------


Instalar Stalker software en Windows (Probado en W7)

Instalar controlador de adaptador USB

Comprobar Puerto COM en Admin de dispositivos 

Abrir 'Stalker Sensor Wizard'

Realizar 'Test Sensor'

En Configuracion avanzada cambiar 'OUTPUT FORMAT' de [COM B] a 'D3'.     Ejemplo  D3 format --> *018.0,375 --> 18 m/s

Probar con muestras de calibracion [probado con 40MPH y 10MPH]

Abrir comunicacion serial [probado con PuTTY en W7]

Teclear [u + 2 + Enter] para cambiar de medida de velocidad de 'MPH' a 'm/s'

Teclear [c + Enter] para visualizar la configuracion.

Cerrar PuTTY, configuracion terminada.

Para leer por serial port En Linux: abrir terminal [tener instalado 'screen'] y poner 'Screen Puertoserie Baudios'. 
Ejemplo: 'Screen /dev/ttyUSB0 115200'
"""



import serial, time, sys

puerto = sys.argv[1]
baudrate = sys.argv[2]

ser = serial.Serial(port= puerto, baudrate= baudrate,
	bytesize= serial.EIGHTBITS,parity = serial.PARITY_NONE,
	 stopbits= serial.STOPBITS_ONE,timeout= 1)



def readlineCR(port):
    rv = ""
    while True:
        ch = port.read()
        rv += ch
        if ch=='\r' or ch=='':
            return rv



try:
	ser.isOpen()
	print("Sensor Conectado")
except:
	print("error")
	exit()

if(ser.isOpen()):
	try:
		ser.write([117, 50 ,13]) #SET U2 [ASCII] TO READ M/S SPEED PARAMETER.
		while(1):
			rcv = readlineCR(ser)	
			#print(rcv[1:6] + " m/s  &  " + rcv[7:11] + " Amplitud Relativa") #D3 MODE OUTPUT
			#print('{},{} {}'.format(rcv[12:14],(rcv[14:15]), ("m/s")))        #B MODE OUTPUT
			
			print(rcv)
	except Exception:
		print("error")
else:
	print("No se pudo conectar al sensor")

